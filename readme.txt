﻿*******************************************************************************
** Component Information
*******************************************************************************

	Component:              AlphabetizeOptionLists
	Author:                 Jonathan Hult
	Website:                http://jonathanhult.com
	Last Updated On:        build_1_20120904

*******************************************************************************
** Overview
*******************************************************************************

	This component uses JavaScript and the YUI library built-in to WebCenter 
	Content	to alphabetize all the option lists on a page.
	
	Dynamichtml includes:
	std_html_head_declarations - Adds JavaScript function to alphabetize 
	option lists
	
	Preference Prompts:
	AlphabetizeOptionLists_ComponentEnabled - Boolean which controls whether 
	option lists are alphabetized
	
*******************************************************************************
** COMPATIBILITY WARNING
*******************************************************************************

	This component was built upon and tested on the version listed below, 
	this is the only version it is "known" to work on, but it may well work 
	on older/newer versions.
	-10.1.3.5.1 (111229) (Build: 7.2.4.105)

*******************************************************************************
** HISTORY
*******************************************************************************

	build_1_20120904
		- Initial component release
